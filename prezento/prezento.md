---
theme: uncover, enable-all-auto-scaling
backgroundColor: #ddffcc
color: #206020
headingDivider: 5
paginate: true
size: 16:9
inlineSVG: true
---
# Представяне на дипломна работа
![bg right](chalks.jpg)
2021-11-14

тема:
[Приложение на свободен софтуер при съставянето на свободен сборник по математика за подготовка за Национално външно оценяване след VII клас](https://lernu-libera.gitlab.io/matematiko-7-nvo/tru-tezo/tezo.pdf)

автор:
Христо Драгостинов Христов
град Габрово

# Начинания
![bg right](logo.svg)
- [свободен сборник](https://lernu-libera.gitlab.io/matematiko-7-nvo/lernolibro/lernolibro.pdf)
- [уеб сайт](https://lernu-libera.gitlab.io/)
- учебни занятия
  - 17 ученика от VI клас
  - 4 дни през лятото
- два пробни изпита

# Структура
![bg right](lernu-libera-proektoj.png)
4 проекта - [изходен код](https://gitlab.com/lernu-libera)

# Ползвани програми
![bg](white)
![bg right](meditate.svg)
[![h:30](gnu.svg) GNU+Linux](https://www.gnu.org/)
[![h:40](latex.svg)](https://www.latex-project.org/)
[![h:30](python.svg)](https://www.python.org/)
[![h:30](maxima.svg) Maxima](https://maxima.sourceforge.io/)
[![h:40](basex.png)](https://basex.org/)
[![h:20](mathjax.svg)](https://www.mathjax.org/)
[![h:30](bash.svg)](https://www.gnu.org/software/bash/)
[![h:30](git.svg)](https://git-scm.com/)
[![h:30](gimp.svg) GIMP](https://www.gnu.org/software/bash/)
[![h:50](image-magick.png) Image Magick](https://www.gnu.org/software/bash/)

# Свободата да
![bg right:62%](gnu-love.png)
1. ползваме
2. променяме
3. споделяме
4. споделяме промените

# Отговор на въпроси на рецензента
- Направете по-детайлна качествена интерпретация на получените резултати на ученицие
- Как продължавате работата си по темата през тази учебна година?

# Мечта
![bg right:76%](framasoft-David-Revoy.jpg)
